<?php

namespace App\DataFixtures;

use App\Entity\Cours;
use App\Entity\Semestre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 2; $i++) {
            $semestre = new Semestre();
            $semestre->setSemestre($i)
                ->setFormation($faker->company);
            for ($y = 0; $y < 10; $y++) {
                $cours = new Cours();
                $cours->setSemestre($semestre)
                    ->setDescription($faker->sentence)
                    ->setNom($faker->name);
                $manager->persist($cours);
            }
            $manager->persist($semestre);
        }
        $manager->flush();
    }
}
